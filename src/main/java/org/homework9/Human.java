package org.homework9;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

public class Human {
    private String name;
    private String surname;
    private long birthDate;
    private int iq;
    private Map<DayOfWeek, List<String>> schedule;
    private Family family;

    static {
        System.out.println("Human class is loaded.");
    }

    {
        System.out.println("A new Human object is created.");
    }

    public Human() {
        this.schedule = new HashMap<>();
    }

    public Human(String name, String surname) {
        this.name = name;
        this.surname = surname;
        this.schedule = new HashMap<>();
    }

    public Human(String name, String surname, String birthDate) {
        this(name, surname);
        if (birthDate == null || birthDate.isEmpty()) {
            this.birthDate = System.currentTimeMillis();
        } else {
            this.birthDate = getBirthDateUnixFromString(birthDate);
        }
        this.schedule = new HashMap<>();
    }

    public Human(String name, String surname, String birthDate, Family family) {
        this(name, surname, birthDate);
        this.family = family;
        this.schedule = new HashMap<>();
    }

    public Human(String name, String surname, String birthDate, int iq, Family family, String... schedule) {
        this(name, surname, birthDate, family);
        this.iq = iq;
        this.schedule = sortScheduleByDays(schedule);
    }

    public Human(String name, String surname, String birthDate, int iq, Family family, Map<DayOfWeek, List<String>> schedule) {
        this(name, surname, birthDate, family);
        this.iq = iq;
        this.schedule = sortScheduleByDays(schedule);
    }

    public Human(String name, String surname, Family family, int iq) {
        this(name, surname);
        this.setFamily(family);
        this.setIQ(iq);
        this.setBirthDate(getBornDate());
        this.schedule = new HashMap<>();
    }

    public long getBirthDateUnixFromString(String birthDateStr) {
        return LocalDate
                .parse(birthDateStr, DateTimeFormatter.ofPattern("dd/MM/yyyy"))
                .atStartOfDay(ZoneId.systemDefault())
                .toInstant()
                .toEpochMilli();
    }

    public Human(String name, String surname, String birthDate, int iq) {
        this(name, surname, birthDate);
        this.setIQ(iq);
        this.schedule = new HashMap<>();
    }

    public long getBirthDateUnix() {
        return this.birthDate;
    }

    public void setBirthDateUnix(long birthDate) {
        this.birthDate = birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = getBirthDateUnixFromString(birthDate);
    }

    public LocalDate getBirthDateLocal() {
        return Instant.ofEpochMilli(this.birthDate)
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
    }

    public String getBirthDate() {
        return getBirthDateLocal().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
    }

    public String getBornDate() {
        return LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
    }

    public void describeAge() {
        if (birthDate <= 0) {
            System.out.println("Birth date not set.");
        }

        LocalDate birthDateLocal = getBirthDateLocal();
        LocalDate currentDate = LocalDate.now();

        Period period = Period.between(birthDateLocal, currentDate);

        StringBuilder result = new StringBuilder();
        result.append("Years: ").append(period.getYears())
                .append(", month: ").append(period.getMonths())
                .append(", days: ").append(period.getDays());
        System.out.println(result);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getIQ() {
        return iq;
    }

    public void setIQ(int iq) {
        this.iq = iq;
    }

    public Map<DayOfWeek, List<String>> getSchedule() {
        return new HashMap<>(schedule);
    }

    public String buildScheduleString(Map<DayOfWeek, List<String>> schedule) {
        return schedule.entrySet().stream()
                .map(entry -> Arrays.toString(new String[]{entry.getKey().name(), String.valueOf(entry.getValue())}))
                .collect(Collectors.joining(", ", "[", "]"));
    }

    void setActivitiesForDays(DayOfWeek day, String activity) {
        // If the day is not present in the schedule, add it with a new list of activities
        schedule.computeIfAbsent(day, k -> new ArrayList<>());

        // Add the new activity to the day's list of activities
        schedule.computeIfPresent(day, (k, activities) -> {
            activities.add(activity);
            return activities;
        });
    }

    public void setSchedule(Map<DayOfWeek, List<String>> scheduleMap) {
        if (scheduleMap.isEmpty()) {
            this.schedule = new HashMap<>();
        } else {
            this.schedule = new HashMap<>();
            scheduleMap.forEach((day, activities) ->
                    this.schedule.put(day, new ArrayList<>(activities))
            );
        }
        System.out.println(this.schedule);
    }

    public void setSchedule(String... schedule) {
        this.schedule = sortScheduleByDays(schedule);
        System.out.println(this.schedule);
    }

    public void addDayToSchedule(DayOfWeek day, String activity) {
        // Add the new activity to the day's list of activities
        day.addActivities(day, activity);

        // Merge the new activity with the existing activities for that day
        List<String> existingActivities = this.schedule.getOrDefault(day, new ArrayList<>());
        existingActivities.add(activity);
        this.schedule.put(day, existingActivities);

        // Print the updated schedule
        System.out.println(this.schedule);
    }

    public Map<DayOfWeek, List<String>> sortScheduleByDays(Map<DayOfWeek, List<String>> schedule) {
        return copyAndSort(schedule);
    }

    private Map<DayOfWeek, List<String>> copyAndSort(Map<DayOfWeek, List<String>> schedule) {
        return schedule.entrySet().stream()
                .sorted(Comparator.comparingInt(entry -> entry.getKey().ordinal()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
    }

    public Map<DayOfWeek, List<String>> sortScheduleByDays(String... schedule) {
        return copyAndSort(schedule);
    }

    private Map<DayOfWeek, List<String>> copyAndSort(String... schedule) {
        Map<DayOfWeek, List<String>> sortedSchedule = new TreeMap<>(Comparator.comparingInt(Enum::ordinal));

        for (int i = 0; i < schedule.length; i += 2) {
            String dayName = schedule[i];
            String[] activities = schedule[i + 1].split(",\\s*");  // Split activities by comma and optional whitespace

            DayOfWeek day = DayOfWeek.valueOf(dayName);
            List<String> dayActivities = sortedSchedule.getOrDefault(day, new ArrayList<>());
            dayActivities.addAll(Arrays.asList(activities));
            sortedSchedule.put(day, dayActivities);
        }

        return sortedSchedule;
    }

    public Family getFamily() {
        return family;
    }

    public boolean setFamily(Family family) {
        if (this.family != null && !this.family.equals(family)) {
            System.out.println("Error: This person is already part of another family.");
            return false;
        }

        this.family = family;

        return true;
    }

    public String greetPet() {
        if (this.getFamily() != null && this.getFamily().getPets() != null && !this.getFamily().getPets().isEmpty()) {
            StringBuilder greetingBuilder = new StringBuilder("Hello, ");
            this.getFamily().getPets().forEach(petMember -> greetingBuilder.append(petMember.getNickname()).append("!"));
            return greetingBuilder.toString();
        } else {
            return "I don't have a pet.";
        }
    }

    public String describePet() {
        StringBuilder resultBuilder = new StringBuilder();
        if (this.family != null && this.family.getPets() != null && !this.family.getPets().isEmpty()) {
            this.family.getPets().forEach(petMember -> {
                Species species = petMember.getSpecies();
                int age = petMember.getAge();
                int cunningLevel = petMember.getTrickLevel();

                String cunningDescription = (cunningLevel > 50) ? "very cunning" : "almost not cunning";

                resultBuilder.append("I have a ").append(species).append(". It is ").append(age)
                        .append(" years old, and it is ").append(cunningDescription).append(".");
            });
        } else {
            resultBuilder.append("I don't have a pet.");
        }
        return resultBuilder.toString().trim();
    }

    public boolean feedPet(boolean isTimeToFeed) {
        if (this.family != null && this.family.getPets() != null) {
            Set<Pet> pets = this.family.getPets();

            if (!pets.isEmpty()) {
                Pet petToFeed = choosePetToFeed(pets);

                if (isTimeToFeed) {
                    System.out.println("Hmm... I will feed " + petToFeed.getNickname());
                    return true;
                } else {
                    Random random = new Random();

                    int randomTrick = random.nextInt(101);
                    int petTrickLevel = petToFeed.getTrickLevel();
                    System.out.println("Random Trick: " + randomTrick);
                    System.out.println("Pet Trick: " + petTrickLevel);

                    if (petTrickLevel >= randomTrick) {
                        System.out.println("Hmm... I will feed " + petToFeed.getNickname());
                        return true;
                    } else {
                        System.out.println("I think " + petToFeed.getNickname() + " is not hungry.");
                        return false;
                    }
                }
            } else {
                System.out.println("I don't have a pet.");
                return false;
            }
        } else {
            System.out.println("I don't have a pet.");
            return false;
        }
    }

    private Pet choosePetToFeed(Set<Pet> pets) {
        return pets.stream().findFirst().orElse(null);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Human)) return false;

        Human human = (Human) o;

        return birthDate == human.birthDate &&
                Objects.equals(name, human.name) &&
                Objects.equals(surname, human.surname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, birthDate);
    }

    @Override
    public String toString() {
        return String.format("%s{name='%s', surname='%s', birthDate=%s, iq=%d, schedule=%s}",
                getClass().getSimpleName(), getName(), getSurname(), getBirthDate(), getIQ(),
                buildScheduleString(sortScheduleByDays(getSchedule())));
    }
}
