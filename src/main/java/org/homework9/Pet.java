package org.homework9;

import java.util.*;
import java.util.stream.Collectors;

public abstract class Pet {
    private int age;
    private Species species = Species.UNKNOWN;
    private String nickname;
    private int trickLevel;
    private Set<String> habits;

    static {
        System.out.println("Pet class is loaded.");
    }

    {
        System.out.println("A new Pet object is created.");
    }

    public Pet(String nickname, int age) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = 0;
        this.habits = new LinkedHashSet<>();
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public Set<String> getHabits() {
        return habits;
    }

    public void setHabits(Set<String> habits) {
        this.habits = habits;
    }

    public abstract void respond();

    public abstract void eat();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Pet)) return false;

        Pet pet = (Pet) o;

        return age == pet.age &&
                Objects.equals(nickname, pet.nickname) &&
                Objects.equals(species, pet.species);
    }

    @Override
    public int hashCode() {
        return Objects.hash(age, nickname, species);
    }

    @Override
    public String toString() {
        return String.format("Pet{%n" +
                        "    species=%s%n" +
                        "    canFly=%s%n" +
                        "    hasFur=%s%n" +
                        "    numberOfLegs=%d%n" +
                        "    nickname=%s%n" +
                        "    age=%d%n" +
                        "    trickLevel=%d%n" +
                        "    habits=%s%n" +
                        "}",
                species != null ? species : Species.UNKNOWN,
                species.canFly(),
                species.hasFur(),
                species.getNumberOfLegs(),
                nickname != null ? nickname : "null",
                age,
                trickLevel,
                habits != null ? habits.stream().collect(Collectors.joining(", ", "[", "]")) : "null");
    }
}
