package homework9Test;

import org.homework9.Family;
import org.homework9.Man;
import org.homework9.Woman;

public class TestUtilities {

    public static Family setUpFamily(String fatherName, String fatherSurname, String fatherBirthDate,
                                     String motherName, String motherSurname, String motherBirthDate) {
        Man father = new Man(fatherName, fatherSurname, fatherBirthDate);
        Woman mother = new Woman(motherName, motherSurname, motherBirthDate);
        return new Family(father, mother);
    }

    public static Family setUpFamily1() {
        return setUpFamily("John", "Doe", "19/10/1970", "Jane", "Doe", "08/01/1975");
    }

    public static Family setUpFamily2() {
        return setUpFamily("Bob", "Smith", "04/11/1975", "Alice", "Smith", "30/11/1980");
    }
}
